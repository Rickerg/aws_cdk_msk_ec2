import msk = require('@aws-cdk/aws-msk');
import * as cdk from '@aws-cdk/core';
import ec2 = require('@aws-cdk/aws-ec2');

export class AwsMskStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const vpc = new ec2.Vpc(this, 'msk-vpc', {
      cidr: '172.31.0.0/16'
    });

   
    const mskCluster = new msk.CfnCluster(this, 'msk-cluster', {
      clusterName: 'msk-cluster',
      brokerNodeGroupInfo: {
        clientSubnets: [
          vpc.privateSubnets[0].subnetId,
          vpc.privateSubnets[1].subnetId
        ],
        instanceType: 'kafka.t3.small',
        brokerAzDistribution: 'DEFAULT'
      },
      numberOfBrokerNodes: 2,
      kafkaVersion: '2.2.1',
      encryptionInfo: {
        encryptionInTransit: {
          clientBroker: 'PLAINTEXT'
        }
      }
    });
  }
}
