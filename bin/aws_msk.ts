#!/usr/bin/env node
import * as cdk from '@aws-cdk/core';
import { AwsMskStack } from '../lib/aws_msk-stack';
import { AwsEC2Stack } from '../lib/aws_ec2';


const app = new cdk.App();
new AwsMskStack(app, 'AwsMskStack');
new AwsEC2Stack(app,'EC2Stack')
