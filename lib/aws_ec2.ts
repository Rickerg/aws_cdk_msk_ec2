import ec2 = require('@aws-cdk/aws-ec2');
import {InstanceClass,InstanceSize,InstanceType,Subnet,SubnetType,RouterType} from '@aws-cdk/aws-ec2';
import { AutoRollbackConfig } from '@aws-cdk/aws-codedeploy';
import iam = require('@aws-cdk/aws-iam');
import cdk = require('@aws-cdk/core');



export class AwsEC2Stack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

   
   // const rb = AutoRollbackConfig;
   // rb.failedDeployment=false;
    const vpc = new ec2.Vpc(this, 'TheVPC',{
        maxAzs: 1,
        cidr:"10.0.0.0/16",
        subnetConfiguration: [{
            cidrMask: 28,
            name: 'public',
            subnetType: SubnetType.PUBLIC,
        }]
      });  
     
      const mysubnet = new Subnet(this, 'mksSubnet',{
        availabilityZone:"us-east-1a",
        vpcId: vpc.vpcId,
        cidrBlock:"10.0.2.0/24",
        mapPublicIpOnLaunch:false
      });
    
    
      
      const mySecurityGroup = new ec2.SecurityGroup(this, 'MKSSecurityGroup', {
        vpc,
        description: 'Allow ssh access to ec2 instances',
        allowAllOutbound: true   // Can be set to false
      });

      mySecurityGroup.addIngressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(22), 'allow ssh access from the world');
      console.log(mySecurityGroup.securityGroupId)
      
      const instanceRole = new iam.Role(this,'ssminstancerole',
      {
        assumedBy: new iam.ServicePrincipal('ec2.amazonaws.com'),
        managedPolicies: [iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AmazonEC2RoleforSSM')],
      });
      const  mskEC2InstanceProfile = new iam.CfnInstanceProfile( this,'mskEC2InstanceProfile',{
          roles: [instanceRole.roleName]
      })
      const linuxImageId = new ec2.AmazonLinuxImage({
        generation: ec2.AmazonLinuxGeneration.AMAZON_LINUX,
        edition: ec2.AmazonLinuxEdition.STANDARD,
        virtualization: ec2.AmazonLinuxVirt.HVM,
        storage: ec2.AmazonLinuxStorage.GENERAL_PURPOSE,
      }).getImage(this).imageId;

     new ec2.CfnInstance(this, "msk_ec2",{
        imageId: linuxImageId,
        subnetId:mysubnet.subnetId,
        securityGroupIds :[mySecurityGroup.securityGroupId],
        keyName:"mskKeypair",
        instanceType: InstanceType.of(InstanceClass.BURSTABLE2, InstanceSize.MICRO).toString(),
        iamInstanceProfile: mskEC2InstanceProfile.ref,
        tags: [{key: "Name", value: "MSK"}],
      })
      new ec2.CfnInstance(this, "springboot_ec2",{
        imageId: linuxImageId,
        subnetId:vpc.publicSubnets[0].subnetId,
        securityGroupIds :[mySecurityGroup.securityGroupId],
        keyName:"mskKeypair",
        instanceType: InstanceType.of(InstanceClass.BURSTABLE2, InstanceSize.MICRO).toString(),
        iamInstanceProfile: mskEC2InstanceProfile.ref,
        tags: [{key: "Name", value: "Springboot"}],
      })
    }
    
}